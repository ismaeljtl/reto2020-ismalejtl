# EDteam - Reto Frontend 2020

Porqué usar Redux para este proyecto y si consideras que Redux no es necesario, también cuéntame por qué?

Mi respuesta:
Redux es una librería que permite el manejo del estado de la aplicación desde cualquier componente del proyecto. Considero que Redux es una gran herramienta a la hora de desarrollar nuestras aplicaciones, pero no hice uso de ella en este reto debido a que para mí el proyecto tiene un corto alcance (es un proyecto que no posee alta complejidad) y que se puede manejar el estado de toda la aplicación sin el uso de esta. 